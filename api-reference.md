FORMAT: 1A
HOST: http://polls.apiblueprint.org/

# krs-service-autocomplete

This service is responsible for providing a list of potential matching civic addresses, given a partial or complete input address.

## Addresses Collection [/addresses]

### Autocomplete and Geocode Address [GET /addresses/best{?q,lat,lon,session,user}]

Returns list of best-matching addresses for the query given by "q". Results are sorted by distance from origin given by "lat" and "lon".

+ Parameters
    + q: `1688%2015` (string, required) - Query to search for. Can be a full address to be geocoded, a partial address to be auto-completed, a point-of-interest, a business name, etc
    + lat: `49.0324915` (number, required) - Origin latitude
    + lon: `-122.8011448` (number, required) - Origin longitude
    + session: `g911fada-4dbc-4ce8-817a-81c9c9af1f53` (UUID, required) - Session identifier. A session starts when a user begins typing and searching for an address. Session ends when the user has selected an address from one of the results, or cancelled the operation.
    + user: `f911fada-4dbc-4ce8-817a-81c9c9af1f52` (UUID, required) - User identifier. This is used to create and maintain the most-recently-used list and should uniquely identify the user using this service.

+ Response 200 (application/json)

        [{
            "fullAddress": "1688 152 St, Surrey, BC",
            "location": {
                "longitude": -123.3708218,
                "latitude": 48.4176983
            },
            "distance": 123
        },{
            "fullAddress": "1688 152 St, Surrey, BC",
            "location": {
                "longitude": -123.4708218,
                "latitude": 48.5176983
            },
            "distance": 156
        }]


### Report Selected Address [POST /addresses/selected]

+ Request (application/json)

        {
            "session": "f911fada-4dbc-4ce8-817a-81c9c9af1f52",
            "user": "f911fada-4dbc-4ce8-817a-81c9c9af1f52",
            "fullAddress": "1688 152 St, Surrey, BC"
        }

+ Response 204

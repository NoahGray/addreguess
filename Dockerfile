FROM node:10.5.0-stretch

MAINTAINER Noah Gray <noahgray@me.com>

RUN apt-get install -y curl autoconf automake libtool pkg-config

ARG NPM_CONFIG_PRODUCTION=false

RUN mkdir -p /opt/libpostal \
    && mkdir -p /opt/libpostal-data \
    && git clone https://github.com/openvenues/libpostal /opt/libpostal \
    && cd /opt/libpostal \
    && ./bootstrap.sh \
    && ./configure --datadir=/opt/libpostal-data \
    && make \
    && make install \
    && ldconfig

WORKDIR /opt/webapp

COPY . .
RUN yarn install
RUN yarn run build

RUN useradd -m myuser
USER myuser

CMD [ "yarn", "run", "start" ]
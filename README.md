# AddreGuess address autocomplete micro-service

This code contains a web server and JS client for an address auto-complete feature. Please see the test files for the best examples of the client.

The service is specifically designed for Vancouver/BC Canada addresses. In addition to using several API's, it saves results for later use. In this way it's an aggregator for several sources of addresses.

> ⚠️　This software may allow the user to perform things that are outside the terms of use of some of the data sources. It is your responsibility to find out what license you have to the services and whether using this code violates them. Namely, this code may save results from various APIs for later use, which some services may prohibit under certain terms.

# Installation

All platforms require the prerequisites of `postgresql` and `postgis`, as well as `node` and `yarn`.

## Non-macOS systems

Please see:

http://postgis.net/install/

https://github.com/openvenues/libpostal

## macOS

### PostGIS

Install Homebrew, if you dont have it already. If you don't have Postgres installed, you can install it through Homebrew as well.

> Warning, if you already have postgresql installed through Homebrew, running either of the commands below could update it to a version that's not compatible with your other databases, and cause postgresql to stop working. You can either try `brew pin postgresql` to prevent postgresql from updating, or use [this guide](https://gist.github.com/giannisp/ebaca117ac9e44231421f04e7796d5ca) to migrate your databases if necessary.

```zsh
brew install postgresql
brew install postgis
```

### libpostal

Install the dependencies with homebrew like so:

```zsh
brew install curl autoconf automake libtool pkg-config
```

Then do:

```zsh
git clone https://github.com/openvenues/libpostal
cd libpostal
./bootstrap.sh
./configure --datadir=[...some dir with a few GB of space...]
make -j4
sudo make install
```

This may take some time and require a lot of space and resources.

## All systems

After installing Postgres and PostGIS, create or enter an existing database and run this query:

> UPDATE: This may no longer be necessary with our fork of TypeORM

```sql
CREATE EXTENSION postgis;
```

Now just see the `sample.env` to know what environmental variables need to be set. `yarn` is required if you wish to use the scripts in `package.json`. Then:

```zsh
yarn install
yarn run build
yarn run start
```

If you are deploying without pre-building (like using a CI), set a `NPM_CONFIG_PRODUCTION=false` environment variable so the dependencies will be there when you build. The `app.json` is meant for Dokku deployment, there is a similar feature in Heroku.

# Heroku deployment

To deploy to Heroku, we need to use it's container registry (Docker reployment) feature. This is because the "slug" of the normal build will be too large. The included `Dockerfile` enables this. Keep in mind, you will not be able to deploy to a free or hobby-tier Heroku, as a lot more RAM is needed.

These are the steps to deploy in the Docker fashion on Heroku.

```bash
heroku container:login
heroku container:push web
heroku container:release web
```

# Development/Contributing

There are `yarn run test` and `yarn run test:watch` scripts for easy development. Please write tests.

## TODO: Update to TypeORM 0.2.8

Currently we use a custom fork of TypeORM for sptial data support. THe feature has been officially merged into TypeORM and will be released with version `0.2.8` according to [this comment](https://github.com/typeorm/typeorm/pull/2423#issuecomment-402244727). Keep an eye out for this release and switch to the main `typeorm@^0.2.8` when it's available.

# Contributors

* Noah Gray [gitlab](https://gitlab.com/NoahGray) [github](https://github.com/oknoah)
import {IncomingMessage as Request} from 'http'
import test from 'tape'
import uuid from 'uuid/v4'
import hasha from 'hasha'

import {getAutocomplete, postSelection} from './server'

export const testUserUuid = 'd53902f6-2d00-470a-b881-2d3aeb699877'

const res = (cb) => ({
  status: () => ({
    send: (data) => {
      cb(data)
    }
  })
})

test('get mock server results', async (t) => {
  const url = new URL('https://AddreGuess.com/addresses/best')
  const req = {}
  req.query = {
    q: '4357 mai',
    user: uuid(),
    lon: -123.102070,
    lat: 49.285120
  }
  await getAutocomplete(req, res((result) => {
    t.ok(result, 'should return results')
    t.ok(Array.isArray(result), 'should be array of results')
    t.ok(result[0].fullAddress, 'should have fullAddress')
    t.ok(result[0].location, 'should have location')
    t.ok(result[0].distance, 'should have distance')
  }))

  t.end()
})

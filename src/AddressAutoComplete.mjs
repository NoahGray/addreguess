// @flow

import mapBoxGeo from '@mapbox/mapbox-sdk/services/geocoding'
import BCGeocoder from 'databc-geocoder'
import hasha from 'hasha'
import {get, uniqBy} from 'lodash'
import {expand} from 'node-postal'
import fetch from 'node-fetch'

import Database, {Place, User} from './Database'

const turf = require('@turf/turf')

const {PELIAS_URL} = process.env

type Props = {
  mapBoxApiKey: string,
  databcApiKey: string,
};

/* This is a boundry box that covers most of "greater vancouver". It's been replaced with localitities in mapbox. */
const bbox = [-123.308527, 49.004921, -122.081220, 49.394692]

const localities = ["Vancouver", "Richmond", "Delta", "White Rock", "Surrey", "Langley", "Maple Ridge", "Pitt Meadows", "Port Coquitlam", "Coquitlam", "New Westminster", "Burnaby", "Port Moody", "North Vancouver", "West Vancouver", "Lions Bay", "Belcarra", "Anmore"]

const database = Database

export default class AddressAutoComplete {
  constructor (props: Props) {
    const mapbox = mapBoxGeo({
      accessToken: props.mapBoxApiKey
    })

    const databc = new BCGeocoder({
      accessToken: props.databcApiKey
    })

    this.getInitialProps()
    this.mapboxClient = mapbox
    this.databcClient = databc
    this.response = []
    /* The actor is the client making the request. */
    this.actor = {}
  }

  /* This normalizes the address and object when new results are added */
  set results (newResults) {
    const necessaryKeys = ['fullAddress', 'properties', 'id', 'location', 'distance', 'siteName', 'normalizedAddress']

    this.response = newResults.map((result) => {
      const remappedResult = {}
      const {properties} = result
      let address = result.fullAddress || result.place_name || result.properties.fullAddress || `${properties.housenumber || ''} ${properties.street || ''} ${properties.locality} ${properties.region_a}, ${properties.country}`

      Object.keys(result).forEach((key) => {
        if (necessaryKeys.includes(key)) {
          remappedResult[key] = result[key]
          return
        }

        switch (key) {
          case 'place_name': {
            address = result[key].replace(/British Columbia.+/g, "BC")
            break
          }
          case 'text': {
            if (!!result[key] && result[key].length > 2) {
              remappedResult.siteName = result[key]
            }
            break
          }
          case 'geometry': {
            remappedResult.location = result[key]
            break
          }
          default:
            break
        }
      })

      if (!!properties && !!properties.name && properties.name.length > 1) {
        remappedResult.siteName = properties.name
      }

      remappedResult.fullAddress = address

      return remappedResult
    })
  }

  get results () {
    const {response: rez} = this
    let res = JSON.parse(JSON.stringify(rez))

    if (!res || !res[0] || !res[0].location) {
      return res || []
    }

    if (res && res[0] && res[0].location.type && this.actor && this.actor.longitude) {
      const userLocation = turf.point([
        this.actor.longitude,
        this.actor.latitude
      ])

      res = res.map((result) => {
        if (result.distance) {
          return result
        }

        return {
          ...result,
          distance: turf.distance(userLocation, result.location)
        }
      })

      res = res.map((result) => {
        if (!result.distance) {
          return result
        }

        return {
          ...result,
          location: Array.isArray(result.location.coordinates) ? {
            longitude: result.location.coordinates[0],
            latitude: result.location.coordinates[1]
          } : result.location
        }
      })

      res = res.sort((a, b) => a.distance > b.distance).slice(0, 9)
    }

    return res
  }

  /* Connect to Postgres */
  async getInitialProps () {
    this.connection = await database.connect
    this.response = []
  }

  /* Gets results from Mapbox and adds them to this.results */
  async mapbox (query: string) {
    let result = {}

    try {
      result = await this.mapboxClient.forwardGeocode({
        autocomplete: true,
        country: 'CA',
        bbox,
        types: ['address'],
        query,
        proximity: [this.actor.longitude, this.actor.latitude],
        limit: 10
      }).send()
    } catch (e) {
      console.log(e)
    }

    const mapboxResults = get(result, 'body.features', [])

    this.results = this.results.concat(mapboxResults)

    return this.results
  }

  /* Gets results from DataBC and adds them to this.results */
  async databc (query: string) {
    let result = {}

    /* Data BC requires at least 3 characters. We skip it if there aren't 3 letters in the query. */
    if (query.match(/[^\d\s]/g).length < 3) {
      return this.results
    }

    /*
      There is another bug that prevents searching by "occupants": https://github.com/bcgov/api-specs/issues/346
    */
    try {
      result = await this.databcClient.addresses({
        autoComplete: true,
        addressString: query,
        locationDescriptor: 'accessPoint',
        interpolation: 'adaptive',
        localities: localities.join(','),
        matchPrecisionNot: 'LOCALITY,PROVINCE',
        matchPrecision: 'OCCUPANT,SITE,UNIT,CIVIC_NUMBER,INTERSECTION,BLOCK,STREET',
        maxResults: 20,
        minScore: 75
      })
    } catch (e) {
      console.log(e)
    }

    this.results = this.results.concat(result.features)

    return this.results
  }

  async normalize () {
    const addresses = await Promise.all(this.results.map(async (result) => {
      const {properties} = result
      const postal = await expand.expand_address(result.fullAddress || fullAddress)

      return {
        ...result,
        normalizedAddress: postal.sort((a, b) => a < b)[0]
      }
    }))

    this.results = uniqBy(addresses, 'normalizedAddress')

    return this.results
  }

  /* Parforms the chaching of new entries in this.results */
  async cache () {
    const getPlaces = async () => {
      return Promise.all(this.results.map(async (features) => {

        if (features instanceof Place) {
          return features
        }

        const placeInstance = new Place()

        const isGeoJson = features.location && features.location.type

        const siteName = features.siteName || (features.properties && features.properties.siteName)

        placeInstance.location = isGeoJson ? features.location : {
          type: 'Point',
          coordinates: [features.location.longitude, features.location.latitude]
        }
        placeInstance.fullAddress = features.fullAddress || features.properties.fullAddress
        placeInstance.normalizedAddress = features.normalizedAddress
        placeInstance.siteName = (siteName && siteName.length > 1) ? siteName : null
        placeInstance.siteId = features.properties ? features.properties.siteId : undefined
        const placeId = hasha(JSON.stringify(placeInstance), {algorithm: 'md5'})
        placeInstance.placeId = placeId
        const exists = await this.connection.manager.findOne(Place, {
          where: { normalizedAddress: placeInstance.normalizedAddress }
        })
        return exists || this.connection.manager.save(placeInstance)
      }))
    }

    try {
      this.results = await getPlaces()
    } catch (e) {
      /* I believe this unique constraint error can be ignored */
      console.error(e.message)
    }

    return this.results
  }

  /* Get user's nearest past selections */
  async user (props: { user: string | number, lat?: number|string, lon?: number|string }) {
    const {user, lat, lon} = props
    if ((lat && lon) && (!this.actor.longitude && !this.actor.latitude)) {
      this.actor.longitude = parseFloat(lon)
      this.actor.latitude = parseFloat(lat)
    }

    const connection = await database.connect
    const query = { "userId": hasha(user, { algorithm: 'md5' })}

    const places = await connection.manager
      .createQueryBuilder(User, 'user')
      .where("user.userId = :userId", query)
      .leftJoinAndSelect('user.selections', 'place')
      .orderBy({"place.location": {
        distance: {
          type: "Point",
          coordinates: [this.actor.longitude, this.actor.latitude]
        },
        order: 'ASC',
        nulls: 'NULLS LAST'
      }})
      .getOne() || {selections: []}

    this.results = this.results.concat(Object.values(places.selections))

    return this.results || places.selections
  }

  /* Make a selection */
  async selection ({ lat, lon, user, id }) {
    const database = this.connection
    const query = { userId: hasha(user, { algorithm: 'md5' }) }
    let actor = await database.manager.findOne(User, query)
    const place = await database.manager.findOne(Place, id)

    actor.selections.push(place)
    await database.manager.save(actor)

    actor = await database.manager.findOne(User, query)
    return actor
  }

  async pelias (q) {
    if (!PELIAS_URL || PELIAS_URL.length <= 1) {
      return
    }

    const url = new URL(`http://${PELIAS_URL}/v1/autocomplete`)
    url.searchParams.append("text", q)
    const results = await fetch(url)
    const response = await results.json()

    if (response.features && response.features.length > 0) {
      this.results = this.results.concat(response.features)
    }

    return this.results
  }

  async query ({q, lat, lon, user}) {
    this.actor.longitude = parseFloat(lon)
    this.actor.latitude = parseFloat(lat)

    /* This should run all 3 data source searches concurrently and speed things up. */
    const searches = ['mapbox', 'databc', 'pelias']
    await Promise.all(searches.map(search => {
      return this[search](q)
    }))

    await this.normalize()
    await this.cache()
    await this.user({user})
    // await this.format()

    return this.results
  }
}

import 'reflect-metadata';
import hasha from 'hasha';
// import { ValueTransformer } from "typeorm/decorator/options/ValueTransformer";
import { ManyToMany, Entity, createConnection, Column, PrimaryGeneratedColumn, JoinTable, PrimaryColumn, BeforeInsert, Index, CreateDateColumn, UpdateDateColumn } from 'typeorm-spatial';
// import wkx from 'wkx'

const { DATABASE_URL } = process.env;

@Entity()
export class Place {
	@PrimaryGeneratedColumn()
	id = undefined;

	@Column({ type: 'text', unique: true })
	placeId = '';

	@Column({ type: 'geography' })
	@Index({ spatial: true })
	location = '';

	@Column({ type: 'text' })
	fullAddress = '';

	@Column({ type: 'text', unique: true })
	@Index()
	normalizedAddress = '';

	@Column({ type: 'text', cascade: true, nullable: true })
	siteId = '';

	@ManyToMany(type => User)
	users = undefined;

	@CreateDateColumn()
	createdAt = '';

	@UpdateDateColumn()
	updatedAt = '';

	/*
    Apiary doesn't include business name. I think many people may search by name.
  */
	@Column({ type: 'text', nullable: true })
	siteName = '';
}

@Entity()
export class User {
	@PrimaryGeneratedColumn()
	id = undefined;

	/* An MD5 hash of user's uuid, as a safety precaution / legal requirement. */
	@PrimaryColumn({
		type: 'varchar',
		length: 32,
		unique: true,
	})
	userId = '';

	@ManyToMany(type => Place, {
		eager: true,
	})
	@JoinTable({ name: 'user_trips' })
	trips = undefined;

	@ManyToMany(type => Place, {
		eager: true,
	})
	@JoinTable({ name: 'user_selections' })
	selections = undefined;

	/*
    For legal/privacy/security I try dissassociate any user identifiers from the places. To be "secure by design" we must keep as little information about a user as possible. TODO: discuss storing favourites locally only.
  */
	@BeforeInsert()
	hashUserId() {
		if (!!this.userId.match(/^[a-f0-9]{32}$/g)) {
			return;
		}

		this.userId = hasha(this.userId, { algorithm: 'md5' });
	}
}

class Database {
	constructor() {
		this.connection = null;
	}

	get connect() {
		if (this.connection) {
			return this.connection;
		}

		try {
			this.connection = createConnection({
				type: 'postgres',
				url: DATABASE_URL,
				entities: [User, Place],
				synchronize: true,
			});
		} catch (e) {
			console.error(e);
			return;
		}

		return this.connection;
	}

	async getConnect() {
		if (this.connection) {
			return this.connection;
		}

		try {
			this.connection = await createConnection({
				type: 'postgres',
				url: DATABASE_URL,
				entities: [User, Place],
				synchronize: true,
			});
		} catch (e) {
			console.error(e);
			return;
		}

		return this.connection;
	}
}

export default new Database();

import test from 'tape'
// import uuid from 'uuid/v4'
import hasha from 'hasha'

import Database from './Database'
import {User} from './Database'

export const testUserUuid = 'd53902f6-2d00-470a-b881-2d3aeb699877'

const db = Database

test('test db connection', async (t) => {
  const connection = await db.connect

  t.ok(connection, 'connection is a thing')
  t.end()
})

test('set and get current user', async (t) => {
  const connection = await db.connect
  const userDb = connection.getRepository(User)

  const newUser = new User()
  newUser.userId = testUserUuid
  const userIdHashed = hasha(newUser.userId, { algorithm: 'md5' })

  await userDb.save(newUser)
  const user = await userDb.findOne({
    where: { userId: userIdHashed }
  })

  t.ok(user, 'user instance exists')
  t.ok(user.userId === userIdHashed, 'user has hash id')
  t.end()
})

// test('select place' => async (t) => {
  
// })

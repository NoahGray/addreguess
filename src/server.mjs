import express from 'express'
import bodyParser from 'body-parser'

import AddressAutoComplete from './AddressAutoComplete'

const app = express()
app.use(bodyParser.json())

export async function getAutocomplete (req, res) {
  let data = {}

  const geoClient = new AddressAutoComplete({
    mapBoxApiKey: process.env.MAPBOX_TOKEN,
    databcApiKey: process.env.DATABC_TOKEN
  })

  try {
    data = await geoClient.query(req.query)
  } catch (e) {
    console.error(e)
    res.status(500).send(e.message)
  }

  return res.status(200).send(data)
}

export async function postSelection (req, res) {
  let data = {}

  const geoClient = new AddressAutoComplete({
    mapBoxApiKey: process.env.MAPBOX_TOKEN,
    databcApiKey: process.env.DATABC_TOKEN
  })

  try {
    data = await geoClient.selection(req.body)
  } catch (e) {
    console.error(e)
    res.status(500).send(e.message)
  }

  res.status(200).send(data)
}

app.get('/addresses/best', getAutocomplete)

app.post('/addresses/selected', postSelection)

app.listen(process.env.PORT || 3333, () => console.log(`server running on port ${process.env.PORT || 3333}`))

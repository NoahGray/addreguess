import test from 'tape'
import uuid from 'uuid/v4'

import AddressAutoComplete from './AddressAutoComplete'

// import Database from './Database'
// import {User, Place} from './Database'
import {testUserUuid} from './Database.test'

// const db = Database

const geoClient = new AddressAutoComplete({
  mapBoxApiKey: process.env.MAPBOX_TOKEN,
  databcApiKey: process.env.DATABC_TOKEN
})

test('test pelias', async (t) => {
  const geo = new AddressAutoComplete({
    mapBoxApiKey: process.env.MAPBOX_TOKEN,
    databcApiKey: process.env.DATABC_TOKEN
  })

  const peliasData = await geo.pelias('mcdonald')

  t.ok(Array.isArray(peliasData), 'should be array of results')
  t.ok(peliasData[0], 'should have at least one item')

  t.end()
})

test('test mapbox', async (t) => {
  const mapboxResult = await geoClient.mapbox('1917 Fern');

  t.ok(Array.isArray(mapboxResult), 'results should be an array')
  t.equals(mapboxResult[0].fullAddress, '1917 Ferndale Street, Vancouver, BC')
  t.end()
})

test('test databc maps', async (t) => {
  const databcResult = await geoClient.databc('1917 fernd')

  t.ok(Array.isArray(databcResult), 'result should be an array')
  t.equals(databcResult[0].fullAddress, '1917 Ferndale Street, Vancouver, BC')
  t.end()
})

test('normalize results', async (t) => {
  const client = new AddressAutoComplete({
    mapBoxApiKey: process.env.MAPBOX_TOKEN,
    databcApiKey: process.env.DATABC_TOKEN
  })
  await client.mapbox('307 hast')
  await client.databc('307 hast')
  const normalized = await client.normalize()

  t.ok(Array.isArray(normalized), 'should return array')
  t.ok(normalized[0].normalizedAddress !== normalized[1].normalizedAddress, 'should not contain duplicate normalized addresses')
  t.end()
})

test('set cache from existing results', async (t) => {
  await geoClient.normalize()
  const cached = await geoClient.cache()

  t.ok(Array.isArray(cached), 'chech cached is array')
  t.equal(cached[0].fullAddress, '1917 Ferndale Street, Vancouver, BC')
  t.end()
})

test('remember selections', async (t) => {
  const actor = await geoClient.selection({ user: testUserUuid, lat: 49.24966, lon: -123.11934, id: 1 })

  t.ok(actor.selections, 'should have selections property')
  t.ok(Array.isArray(actor.selections), 'should have selections array')
  t.ok(actor.selections.length > 0, 'should have items in array')
  t.ok(actor.selections[0].location, 'should have item with location property')
  t.end()
})

test('get user selections sorted', async (t) => {
  const results = await geoClient.user({ user: testUserUuid, lat: 49.24966, lon: -123.11934 })

  t.ok(results, 'should return something')
  t.ok(Array.isArray(results), 'should have array of selections')
  t.end()
})

test('complete query', async (t) => {
  const response = await geoClient.query({
    q: '6078 main s',
    lat: 49.276044,
    lon: -123.099531,
    session: uuid(),
    user: testUserUuid
  })

  t.ok(Array.isArray(response), 'should respond with array')
  t.ok(response.length <= 10, 'should not have more than 10 results')
  t.ok(response[0].fullAddress, 'should have fullAddress')
  t.ok(response[0].location, 'should have location')
  t.ok(response[0].distance, 'should have distance')
  t.end()
})

test('complete occupant name query', async (t) => {
  const response = await geoClient.query({
    q: 'mcdonald',
    lat: 49.276044,
    lon: -123.099531,
    session: uuid(),
    user: testUserUuid
  })

  t.ok(Array.isArray(response), 'should respond with array')
  t.ok(response.length <= 20, 'should not have more than 10 results')
  t.ok(response[0].fullAddress, 'should have fullAddress')
  t.ok(response[0].location, 'should have location')
  t.ok(response[0].distance, 'should have distance')
  t.end()
})

